package anchor_user

import (
	"bitbucket.org/kawan-cicil-skeleton/entity"
)

type Repository interface {
	Test(req entity.AnchorUser) (resp entity.AnchorUser, err error)
}
