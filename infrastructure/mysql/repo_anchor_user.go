package mysql

import (
	"bitbucket.org/kawan-cicil-skeleton/domain/anchor_user"
	"bitbucket.org/kawan-cicil-skeleton/entity"
	"bitbucket.org/kawan-cicil-skeleton/shared/database"
)

type testRepo struct {
	db *database.Database
}

func SetupTestRepo(db *database.Database) anchor_user.Repository {
	return &testRepo{db: db}
}

func (a *testRepo) Test(req entity.AnchorUser) (resp entity.AnchorUser, err error) {
	operation := a.db.
		Set("gorm:association_autoupdate", true).
		Model(&req).
		Save(&req)
	return resp, operation.Error
}