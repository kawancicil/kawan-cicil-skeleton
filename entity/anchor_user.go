package entity

import (
	"bitbucket.org/kawan-cicil-skeleton/shared/constant"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"time"
)

type AnchorUser struct {
	ID           int64   `gorm:"column:id;primary_key"`
	CreatedBy    string  `gorm:"column:created_by"`
	CreatedDate  int64   `gorm:"column:created_date"`
	ModifiedBy   string  `gorm:"column:modified_by"`
	ModifiedDate int64   `gorm:"column:modified_date"`
	SecureId     string  `gorm:"column:secure_id"`
	Name         string  `gorm:"column:name"`
	PhoneNumber  string  `gorm:"column:phone_number"`
	Email        string  `gorm:"column:email"`
	BorrowerID   *string `gorm:"column:borrower_id"`
	AnchorID     int64   `gorm:"column:anchor_id"`
}

func (AnchorUser) TableName() string {
	return constant.EntityAnchorUser // this name table
}

func (l *AnchorUser) BeforeCreate(scope *gorm.Scope) error {
	_ = scope.SetColumn("SecureId", uuid.New().String())
	_ = scope.SetColumn("CreatedDate", time.Now().Unix())
	_ = scope.SetColumn("ModifiedDate", time.Now().Unix())
	return nil
}
