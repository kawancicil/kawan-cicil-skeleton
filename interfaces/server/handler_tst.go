package server

import (
	"bitbucket.org/kawan-cicil-skeleton/shared/constant"
	"bitbucket.org/kawan-cicil-skeleton/usecase/test"
	"github.com/gin-gonic/gin"
	"net/http"
)

type testHandler struct {
	service test.Service
}

func newAdminHandler(service test.Service) *testHandler {
	return &testHandler{service: service}
}

func (h *testHandler) Test() func(ctx *gin.Context) {
	return func(ctx *gin.Context) {
		res, err := h.service.Test("test")
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"error_description": err.Error(), "error": constant.EntityErrBadRequest})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{"message": constant.SuccessMsg, "data": res})
	}
}