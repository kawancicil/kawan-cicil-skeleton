package server

// start framework
import (
	"flag"
	"fmt"

	"github.com/gin-gonic/gin"

	"bitbucket.org/kawan-cicil-skeleton/interfaces/container"
	"bitbucket.org/kawancicil/kawan-cicil-logger"
	"bitbucket.org/kawancicil/kawan-cicil-logger/middleware"
)

func StartApp(container container.Container) *gin.Engine {
	addr := flag.String("addr: ", container.Config.App.Addr, "Address to listen and serve")
	app := gin.Default()

	//Setup logger
	log := logger.New(container.Config.Logger)
	middleware.SetupMiddlewareLogger(app, log)

	// Setup Handler
	handler := setupHandler(container)

	//Setup Router
	setupRouter(app, handler)

	fmt.Println(app.Run(*addr))
	return app
}
