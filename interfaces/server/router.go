package server

import (
	"github.com/gin-gonic/gin"
)

func setupRouter(app *gin.Engine, handler *handler) {
	test 		:= app.Group("/test")

	v1 := test.Group("/api/v1")
	v1.GET("/test", handler.testHandler.Test())
}
