package server

import "bitbucket.org/kawan-cicil-skeleton/interfaces/container"

type handler struct {
	testHandler    *testHandler
}

func setupHandler(container container.Container) *handler {
	testHandler := newAdminHandler(container.TestSvc)
	return &handler{
		testHandler: testHandler,
	}
}

