package container

import (
	"bitbucket.org/kawan-cicil-skeleton/infrastructure/mysql"
	"bitbucket.org/kawan-cicil-skeleton/shared/config"
	"bitbucket.org/kawan-cicil-skeleton/shared/database"
	"bitbucket.org/kawan-cicil-skeleton/usecase/test"
)

type Container struct {
	Config  *config.Config
	TestSvc test.Service
}

func SetupContainer() (out Container) {
	//Setup config
	cfg := config.SetupConfig()

	// setup database connection
	db := database.SetupDatabase(cfg)

	// setup infra
	testRepo := mysql.SetupTestRepo(db)

	// setup service
	serviceTest := test.NewTestService(cfg, testRepo)

	return Container{
		Config: cfg,
		TestSvc: serviceTest,
	}
}
