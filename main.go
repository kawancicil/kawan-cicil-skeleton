package main

import (
	"bitbucket.org/kawan-cicil-skeleton/interfaces/container"
	"bitbucket.org/kawan-cicil-skeleton/interfaces/server"
)

func main() {
	server.StartApp(container.SetupContainer())
}