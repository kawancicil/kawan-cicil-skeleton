module bitbucket.org/kawan-cicil-skeleton

go 1.13

require (
	bitbucket.org/kawancicil/kawan-cicil-logger v0.0.0-20201111190003-8aeceff316bf
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/google/uuid v1.1.1
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/jinzhu/gorm v1.9.16
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/spf13/viper v1.7.1
	github.com/tebeka/strftime v0.1.5 // indirect
)
