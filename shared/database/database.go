package database

import (
	"bitbucket.org/kawan-cicil-skeleton/shared/config"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Database struct {
	*gorm.DB
}

func SetupDatabase(config *config.Config) *Database {
	fmt.Println("Try Setup Database ...")

	url := fmt.Sprintf(config.DB.Datasource)
	dialect := fmt.Sprintf(config.DB.Dialect)
	db, err := gorm.Open(dialect, url)

	// Enable Logger, show detailed log
	db.LogMode(true)

	//db.DB().SetMaxIdleConns(config.MaxIdleConns)
	//db.DB().SetMaxOpenConns(config.MaxOpenConns)

	if err != nil {
		panic(err)
	}

	return &Database{db,}
}
