package constant

var (
	EntityErrBadRequest = "bad_request"
	SuccessMsg          = "Success"
	InvalidParameterMsg = "Invalid Parameter"
	SystemErrorMsg      = "System Error"
)
