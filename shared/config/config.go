package config

import (
	"bitbucket.org/kawancicil/kawan-cicil-logger"
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"time"
)

type Config struct {
	App            AppConfig            `mapstructure:"app" json:"app" yaml:"app"`
	DB             DatabaseConfig       `mapstructure:"db" json:"db" yaml:"db"`
	Otp            OtpConfig            `mapstructure:"otp" json:"otp" yaml:"otp"`
	Logger         logger.Options       `mapstructure:"logger" yaml:"logger"`
	Notification   NotificationConfig   `mapstructure:"notification" json:"notification"`
	OAuth          OAuthConfig          `mapstructure:"oauth" yaml:"oauth"`
	User           UserConfig           `mapstructure:"user" json:"user" yaml:"user"`
	Internal       InternalConfig       `mapstructure:"internal" yaml:"internal"`
	AlibabaOSS     AlibabaOSSConfig     `mapstructure:"alibabaOSS" json:"alibabaOSS" yaml:"alibabaOSS"`
	PaymentService PaymentServiceConfig `mapstructure:"payment_service" json:"payment_service"`
	MPIN           MPINConfig           `mapstructure:"mpin" json:"mpin"`
	IziData         IziDataConfig         `mapstructure:"iziData" yaml:"iziData"`
	PropertyService PropertyServiceConfig `mapstructure:"property_service" yaml:"property_service"`
}

//AlibabaOSSConfig cloud config
type AlibabaOSSConfig struct {
	Endpoint      string `mapstructure:"endpoint" yaml:"endpoint"`
	AccessKeyID   string `mapstructure:"accesskeyid" yaml:"accesskeyid"`
	AccessSecret  string `mapstructure:"accesssecret" yaml:"accesssecret"`
	Bucket        string `mapstructure:"bucket" yaml:"bucket"`
	FileUploadDir string `mapstructure:"fileuploaddir" yaml:"fileuploaddir"`
}

//NotificationConfig to specify micro-service application configuration
type NotificationConfig struct {
	Username       string        `mapstructure:"username" yaml:"username"`
	Password       string        `mapstructure:"password" yaml:"password"`
	Timeout        time.Duration `mapstructure:"timeout" yaml:"timeout"`
	SmsUrl         string        `mapstructure:"sms_url" yaml:"sms_url"`
	EmailUrl       string        `mapstructure:"email_url" yaml:"email_url"`
	EmailHeader    string        `mapstructure:"email_header" yaml:"email_header"`
	RmDashboardUrl string        `mapstructure:"rm_dashboard_url" yaml:"rm_dashboard_url"`
}

type OAuthConfig struct {
	ClientId       string `mapstructure:"clientid" yaml:"clientid"`
	ClientSecret   string `mapstructure:"clientsecret" yaml:"clientsecret"`
	GrantType      string `mapstructure:"granttype" yaml:"granttype"`
	ProvisionKey   string `mapstructure:"provisionkey" yaml:"provisionkey"`
	TokenURL       string `mapstructure:"tokenurl" yaml:"tokenurl"`
	DeleteTokenURL string `mapstructure:"deletetokenurl" yaml:"deletetokenurl"`
}

type AppConfig struct {
	HttpsEnabled           bool   `mapstructure:"httpsenabled" json:"https_enabled" ymal:"httpsenabled"`
	Version                string `mapstructure:"version" json:"version" ymal:"version"`
	Addr                   string `mapstructure:"addr" json:"addr" ymal:"addr"`
	Mode                   string `mapstructure:"mode" json:"mode" ymal:"mode"`
	MaxMultipartMemory     int    `mapstructure:"max_multipart_memory" json:"max_multipart_memory" ymal:"max_multipart_memory"`
	BaseURL                string `mapstructure:"base_url" yaml:"base_url"`
	AndroidAppLinkBorrower string `mapstructure:"android_app_borrower_link" yaml:"android_app_borrower_link"`
	AndroidAppLinkLender   string `mapstructure:"android_app_lender_link" yaml:"android_app_lender_link"`
	IOSAppLinkBorrower     string `mapstructure:"ios_app_link_borrower" yaml:"ios_app_link_borrower"`
	IOSAppLinkLender       string `mapstructure:"ios_app_link_lender" yaml:"ios_app_link_lender"`
	ViewPath               string `mapstructure:"view_path" yaml:"view_path"`
	EmailTemplates         string `mapstructure:"email_templates" yaml:"email_templates"`
	SupportPhone           string `mapstructure:"support_phone" yaml:"support_phone"`
	SupportEmail           string `mapstructure:"support_email" yaml:"support_email"`
}

//UserConfig to specify application configuration constants
type UserConfig struct {
	EmailVerifyLink              string `mapstructure:"emailverifylink" yaml:"emailverifylink"`
	ResetPasswordLink            string `mapstructure:"resetpasswordlink" yaml:"resetpasswordlink"`
	ResetPasswordMailSubject     string `mapstructure:"resetpasswordmailsubject" yaml:"resetpasswordmailsubject"`
	ResetPasswordDefaultInitials string `mapstructure:"resetpassworddefaultinitials" yaml:"resetpassworddefaultinitials"`
}

// OtpConfig is the OTP Config
type OtpConfig struct {
	ExpiryTime string `mapstructure:"expiry_time" yaml:"expiry_time"`
	Retries    int    `mapstructure:"retries" yaml:"retries"`
}

type DatabaseConfig struct {
	Dialect      string
	Datasource   string            `mapstructure:"datasource" yaml:"datasource" json:"datasource"`
	MaxIdleConns int               `mapstructure:"max_idle_conns" yaml:"max_idle_conns" json:"max_idle_conns"`
	MaxOpenConns int               `mapstructure:"max_open_conns" yaml:"max_open_conns" json:"max_open_conns"`
	Table        map[string]string `mapstructure:"table" yaml:"table" json:"table"`
}

type PaymentServiceConfig struct {
	BaseUri string `mapstructure:"base_uri" yaml:"base_uri"`
}

type MPINConfig struct {
	Length int `mapstructure:"length" yaml:"length"`
}

type IziDataConfig struct {
	AccessKey               string `mapstructure:"access_key" yaml:"access_key"`
	SecretKey               string `mapstructure:"secret_key" yaml:"secret_key"`
	IziDataOcrUri           string `mapstructure:"izi_data_ocr_uri" yaml:"izi_data_ocr_uri"`
	IziDataCreditFeatureUri string `mapstructure:"izi_data_credit_feature_uri" yaml:"izi_data_credit_feature_uri"`
}

type PropertyServiceConfig struct {
	BaseUri string `mapstructure:"base_uri" yaml:"base_uri"`
}

var (
	App             AppConfig
	DB              DatabaseConfig
	Otp             OtpConfig
	Logger          logger.Options
	Notification    NotificationConfig
	OAuth           OAuthConfig
	User            UserConfig
	Internal        InternalConfig
	AlibabaOSS      AlibabaOSSConfig
	PaymentService  PaymentServiceConfig
	MPIN            MPINConfig
	IziData         IziDataConfig
	PropertyService PropertyServiceConfig
)

// InternalConfig is the APIs config
type InternalConfig struct {
	BaseUri           string `mapstructure:"base_uri" yaml:"base_uri"`
	BasicAuthUsername string `mapstructure:"basic_auth_username" yaml:"basic_auth_username"`
	BasicAuthPassword string `mapstructure:"basic_auth_password" yaml:"basic_auth_password"`
	KpayUri           string `mapstructure:"uri_kpay" yaml:"uri_kpay"`
	LoanPublisherUri  string `mapstructure:"uri_loan_publisher" yaml:"uri_loan_publisher"`
}

func SetupConfig() (config *Config) {
	fmt.Println("Setup Config ....")
	config = &Config{}

	viper.SetConfigName("config.yml") // name of config file (without extension)
	viper.SetConfigType("yaml")       // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("./resource/")
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	err = viper.Unmarshal(config)
	if err != nil {
		panic(err)
	}

	js, _ := json.Marshal(config)
	fmt.Println(string(js))

	App = config.App
	DB = config.DB
	Logger = config.Logger
	Otp = config.Otp
	OAuth = config.OAuth
	User = config.User
	Internal = config.Internal
	AlibabaOSS = config.AlibabaOSS
	PaymentService = config.PaymentService
	MPIN = config.MPIN
	Notification = config.Notification
	IziData = config.IziData
	PropertyService = config.PropertyService
	return config
}

func (c *Config) GetHttpAddress() string {
	return c.App.Addr
}
