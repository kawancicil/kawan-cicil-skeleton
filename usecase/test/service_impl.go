package test

import (
	"bitbucket.org/kawan-cicil-skeleton/domain/anchor_user"
	"bitbucket.org/kawan-cicil-skeleton/shared/config"
)

type service struct {
	cfg        *config.Config
	anchorUser anchor_user.Repository
}

func NewTestService(cfg *config.Config,
	anchorUser anchor_user.Repository) Service {
	return &service{
		anchorUser: anchorUser,
		cfg:        cfg,
	}
}

func (s *service) Test(req string) (resp string, err error) {
	return "Success", nil
}