package test

type Service interface {
	Test(req string) (resp string, err error)
}
